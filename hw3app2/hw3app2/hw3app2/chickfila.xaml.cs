﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw3app2
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class chickfila : ContentPage
	{
        int votes = 0;

        public chickfila()
        {
            InitializeComponent();
            VotesLabel.Text = "Chick-fil-A votes: " + votes.ToString();
        }

        private void Selection_Clicked(object sender, EventArgs e)
        {
            votes++;
            VotesLabel.Text = "Chick-fil-A votes: " + votes.ToString();
        }

        private void ClearButton_Clicked(object sender, EventArgs e)
        {
            votes = 0;
            VotesLabel.Text = "Chick-fil-A votes: " + votes.ToString();
        }
    }
}